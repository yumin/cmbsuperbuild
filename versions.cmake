superbuild_set_revision(boost
  URL     "https://midas3.kitware.com/midas/download/bitstream/457867/boost_1_60_0.tar.bz2"
  URL_MD5 65a840e1a0b13a558ff19eeb2c4f0cbe)

# XXX: When updating this, update the version number in CMakeLists.txt as well.
# Using a Pre ParaView 5.2 version (on master)
#
set(paraview_revision 3c63addd2138300ea2a41b0960e8ce99989632e4)
if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(shiboken
  # https://github.com/OpenGeoscience/shiboken.git
  URL     "http://www.paraview.org/files/dependencies/shiboken-110e45fa9d873afea4d7ae47da3fb678b1831a21.tar.bz2"
  URL_MD5 53e71b32964b5daf38b45e1679623e48)

superbuild_set_external_source(smtk
  "https://gitlab.kitware.com/cmb/smtk.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_external_source(cmb
  "https://gitlab.kitware.com/cmb/cmb.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG        origin/master)

superbuild_set_revision(vxl
  # https://github.com/judajake/vxl.git
  URL     "http://www.paraview.org/files/dependencies/vxl-44433e4bd8ca3eabe4e5441444bf2a050d689d45.tar.bz2"
  URL_MD5 dfff6d1958334cbbadb4edbf8c3f4cb6)

superbuild_set_revision(opencv
  # https://github.com/opencv/opencv.git
  # master as of Aug 11 2016
  URL     "http://www.paraview.org/files/dependencies/opencv-74e997f15b20600895896e1a837a0bb3cbf7f07b.tar.bz2"
  URL_MD5 b33767f505bb0e67ffb25d33f5301fa1)

superbuild_set_revision(opencvcontrib
  # https://github.com/opencv/opencv_contrib.git
  # master as of Aug 11 2016
  URL     "http://www.paraview.org/files/dependencies/opencv-contrib-73459049a323af2c88e89037b45fadb7085020fb.tar.bz2"
  URL_MD5 ce88b2b192bc4018ef39cbafcc8d77b3)

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  # https://github.com/robertmaynard/zeromq4-x.git
  URL     "http://www.paraview.org/files/dependencies/zeromq4-8f612f55e29c75c8d3d0790626a1e3de0186cd1d.tar.bz2"
  URL_MD5 7c1a954aa05a882b5946dfe4b4b42219)

# Use remus from Fri Jul 29 15:25:33 2016 -0400
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        378c70d5f7ebd3323e724f60b4dbc2ba2dfc3011)

superbuild_set_revision(kml
  URL     "http://vtk.org/files/support/libkml_fa6c7d8.tar.gz"
  URL_MD5 261b39166b18c2691212ce3495be4e9c)

superbuild_set_revision(gdal
  # https://github.com/judajake/gdal-svn.git
  URL     "http://www.paraview.org/files/dependencies/gdal-95abc689c1e356b3fcb0a2b71f53f4f23917b56c.tar.bz2"
  URL_MD5 74f67d309e276f742ae52dcc0f072e18)

superbuild_set_revision(moab
  # https://bitbucket.org/mathstuf/moab.git
  URL     "http://www.paraview.org/files/dependencies/moab-ca9ab9e13928f5fbba884f9167111d79cc84781e.tar.bz2"
  URL_MD5 f593bba902f8db16aeb83f01a01f2102)

superbuild_set_revision(triangle
  # https://github.com/robertmaynard/triangle.git
  URL     "http://www.paraview.org/files/dependencies/triangle-4c20820448cdfa27f968cfd7cb33ea5b9426ad91.tar.bz2"
  URL_MD5 9a016bc90f1cdff441c75ceb53741b11)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsetuptools
  URL     "https://pypi.python.org/packages/45/5e/79ca67a0d6f2f42bfdd9e467ef97398d6ad87ee2fa9c8cdf7caf3ddcab1e/setuptools-23.0.0.tar.gz"
  URL_MD5 100a90664040f8ff232fbac02a4c5652)

superbuild_set_revision(pythongirderclient
  URL     "https://pypi.python.org/packages/source/g/girder-client/girder-client-1.1.2.tar.gz"
  URL_MD5 4cd5e0cab41337a41f45453d25193dcf)

superbuild_set_revision(ftgl
  # https://github.com/ulrichard/ftgl.git
  URL     "http://www.paraview.org/files/dependencies/ftgl-dfd7c9f0dee7f0059d5784f3a71118ae5c0afff4.tar.bz2"
  URL_MD5 16e54c7391f449c942f3f12378db238f)

superbuild_set_revision(oce
  # https://github.com/mathstuf/oce.git
  URL     "http://www.paraview.org/files/dependencies/oce-9b4646a11c7d9be65a6c2df0ade6604cc91b1fa4.tar.bz2"
  URL_MD5 adea5bd5a7510c7da58755ca7d964319)

superbuild_set_revision(cgm
  # https://bitbucket.org/mathstuf/cgm.git
  URL     "http://www.paraview.org/files/dependencies/cgm-fd563100bc4d194721d5a2be8775a613738326f6.tar.bz2"
  URL_MD5 f875003c15ef43f3bfb4da8ded95d73c)
