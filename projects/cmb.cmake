set(cmb_smtk_libdir lib)
if (WIN32)
  set(cmb_smtk_libdir bin)
endif ()

set(cmb_extra_optional_dependencies)
if (USE_NONFREE_COMPONENTS)
  list(APPEND cmb_extra_optional_dependencies
    triangle)
endif ()

superbuild_add_project(cmb
  DEVELOPER_MODE
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost remus vxl kml gdal qt4 python paraview shiboken
          cmbworkflows zeromq opencv
          smtk # XXX(smtk): until the submodule works properly
  DEPENDS_OPTIONAL moab smtk ${cmb_extra_optional_dependencies}
                   cxx11
  CMAKE_ARGS
    ${extra_cmake_args}
    -DKML_DIR:PATH=<INSTALL_DIR>
    -DGDAL_DIR:PATH=<INSTALL_DIR>
    -DCMB_TEST_DATA_ROOT:PATH=${CMB_TEST_DATA_ROOT}

    #specify semi-colon separated paths for session plugins
    -DCMB_TEST_PLUGIN_PATHS:STRING=<INSTALL_DIR>/${cmb_smtk_libdir}
    #specify what mesh workers we should build
    -DBUILD_TRIANGLE_MESH_WORKER:BOOL=${triangle_enabled}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    # This should be off by default because vtkCmbMoabReader in discrete
    # session may only be needed for debugging purpose
    -DSMTK_ENABLE_MOAB_DISCRETE_READER:BOOL=OFF

    -DSMTK_USE_SYSTEM_MOAB:BOOL=${moab_enabled}
    -DMOAB_INCLUDE_DIR:PATH=<INSTALL_DIR>/include

    # SMTK bits
    -DUSE_SYSTEM_SMTK:BOOL=${smtk_enabled}
    -DENABLE_HDF5:BOOL=${hdf5_enabled}

    -DCMB_SUPERBUILD_DEVELOPER_ROOT:PATH=<INSTALL_DIR>

    -DCMAKE_INSTALL_LIBDIR:STRING=${cmb_smtk_libdir})

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()
