superbuild_add_project(opencv
  DEPENDS
    python qt4 opencvcontrib numpy
  CMAKE_ARGS
    -DOPENCV_EXTRA_MODULES_PATH:PATH=${CMAKE_CURRENT_BINARY_DIR}/opencvcontrib/src/modules
    -DBUILD_PERF_TESTS:BOOL=OFF
    -DBUILD_TESTS:BOOL=OFF
    -DBUILD_opencv_flann:BOOL=OFF
    -DBUILD_opencv_hdf:BOOL=OFF
    -DBUILD_opencv_dnn:BOOL=OFF
    -DWITH_CUDA:BOOL=OFF
    -DWITH_GTK:BOOL=OFF
    -DWITH_FFMPEG:BOOL=OFF
    -DWITH_VTK:BOOL=OFF
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib)

if (WIN32)
  superbuild_add_extra_cmake_args(
    -DOpenCV_DIR:PATH=<INSTALL_DIR>/share)
endif ()
